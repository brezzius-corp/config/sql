CREATE TABLE live (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    latitude DOUBLE NOT NULL,                           -- Latitude de l'utilisateur
    longitude DOUBLE NOT NULL,                          -- Longitude de l'utilisateur
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE log (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    date DATETIME NOT NULL,                             -- Date de la requête
    ip VARCHAR(15) NOT NULL,                            -- IP du client
    port SMALLINT(5) UNSIGNED NOT NULL,                 -- Port entrant du serveur
    mac VARCHAR(17) NOT NULL,                           -- Addresse MAC du client
    method VARCHAR(7) NOT NULL,                         -- Méthode HTTP utilisée
    uri VARCHAR(2048) NOT NULL,                         -- URI demandée par le client
    version VARCHAR(8) NOT NULL,                        -- Version du protocole HTTP utilisé
    mimetype VARCHAR(22) NOT NULL,                      -- Mimetype de la ressource demandée
    code SMALLINT(5) UNSIGNED NOT NULL,                 -- Code retour de la requête envoyé au client
    len SMALLINT(5) UNSIGNED NOT NULL,                  -- Longueur de la requête cliente
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE log_header (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    lid INTEGER(10) UNSIGNED NOT NULL,                  -- Identifiant de la requête HTTP (table log)
    type TINYINT(3) UNSIGNED NOT NULL,                  -- Type de l'en-tête (1 : En-tête HTTP / 2 : Cookie / 3 : POST / 4 : GET)
    name VARCHAR(255) NOT NULL,                         -- Clé de l'en-tête
    value VARCHAR(255) NOT NULL,                        -- Valeur de l'en-tête
    PRIMARY KEY(id)
) ENGINE=INNODB;

