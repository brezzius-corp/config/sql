CREATE TABLE canyoning (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    place VARCHAR(128) NOT NULL,                        -- Ville 
    river VARCHAR(128) NOT NULL,                        -- Rivière
    iele SMALLINT(4) NOT NULL,                          -- Altitude de départ (en m)
    elevation SMALLINT(4) NOT NULL,                     -- Dénivelé négatif (en m)
    distance SMALLINT(4) NOT NULL,                      -- Distance (en m)
    vgrade TINYINT(1) NOT NULL,                         -- Difficulté verticale
    agrade TINYINT(1) NOT NULL,                         -- Difficulté aquatique
    loophole TINYINT(1) NOT NULL,                       -- Echappatoire
    waterfall SMALLINT(3) DEFAULT NULL,                 -- Hauteur de la plus grande cascade (en m)
    abseiling TINYINT(1) DEFAULT NULL,                  -- Rappel (NULL : Non / 0 : Facultatif / 1 : Obligatoire)
    tabseiling TEXT DEFAULT NULL,                       -- Commentaire sur le rappel
    ironway TINYINT(1) DEFAULT NULL,                    -- Via-Ferrata (NULL : Non / 0 : Facultatif / 1 : Obligatoire)
    tironway TEXT DEFAULT NULL,                         -- Commentaire sur la Via-Ferrata
    equipment TINYINT(1) NOT NULL,                      -- Qualité de l'équipement
    risk TINYINT(1) NOT NULL,                           -- Risque du Canyon
    latitude DECIMAL(8, 6) NOT NULL,                    -- Latitude du point de départ
    longitude DECIMAL(8, 6) NOT NULL,                   -- Longitude du point de départ
    approach VARCHAR(255) NOT NULL,                     -- GPX de la marche d'approche
    route VARCHAR(255) NOT NULL,                        -- GPX du Canyon
    back VARCHAR(255) NOT NULL,                         -- GPX de la marche retour
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE climbing (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    place VARCHAR(128) NOT NULL,                        -- Ville
    name VARCHAR(64) NOT NULL,                          -- Nom de la voie
    iele SMALLINT(4) NOT NULL,                          -- Altitude de départ (en m)
    elevation SMALLINT(4) NOT NULL,                     -- Hauteur du rocher (en m)
    grade VARCHAR(3) NOT NULL,                          -- Cotation
    equipment TINYINT(1) NOT NULL,                      -- Qualité de l'équipement
    rock TINYINT(1) NOT NULL,                           -- Qualité du rocher
    risk TINYINT(1) NOT NULL,                           -- Risque en cas de chute
    latitude DECIMAL(8, 6) NOT NULL,                    -- Latitude du point de départ
    longitude DECIMAL(8, 6) NOT NULL,                   -- Longitude du point de départ
    approach VARCHAR(255) NOT NULL,                     -- GPX de la marche d'approche
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE ridge (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    place VARCHAR(128) NOT NULL,                        -- Ville
    chain VARCHAR(128) NOT NULL,                        -- Massif montagneux
    side VARCHAR(2) NOT NULL,                           -- Versant
    name VARCHAR(64) NOT NULL,                          -- Nom de l'arête
    iele SMALLINT(4) NOT NULL,                          -- Altitude de départ (en m)
    elevation SMALLINT(4) NOT NULL,                     -- Dénivelé positif ou négatif (en m)
    distance SMALLINT(4) NOT NULL,                      -- Distance (en m)
    grade VARCHAR(3) NOT NULL,                          -- Cotation
    loophole TINYINT(1) NOT NULL,                       -- Echappatoire
    abseiling TINYINT(1) DEFAULT NULL,                  -- Rappel (NULL : Non / 0 : Facultatif / 1 : Obligatoire)
    tabseiling TEXT DEFAULT NULL,                       -- Commentaire sur le rappel
    equipment TINYINT(1) NOT NULL,                      -- Qualité de l'équipement
    rock TINYINT(1) NOT NULL,                           -- Qualité du rocher
    risk TINYINT(1) NOT NULL,                           -- Risque en cas de chute
    latitude DECIMAL(8, 6) NOT NULL,                    -- Latitude du point de départ
    longitude DECIMAL(8, 6) NOT NULL,                   -- Longitude du point de départ
    approach VARCHAR(255) NOT NULL,                     -- GPX de la marche d'approche
    route VARCHAR(255) NOT NULL,                        -- GPX de l'arête
    back VARCHAR(255) NOT NULL,                         -- GPX de la marche retour
    PRIMARY KEY(id)
) ENGINE=INNODB;

