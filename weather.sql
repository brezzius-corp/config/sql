CREATE TABLE temperature (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    date DATETIME NOT NULL,                             -- Date de la capture
    value DECIMAL(3, 1) DEFAULT NULL,                   -- Valeur
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE pressure (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    date DATETIME NOT NULL,                             -- Date de la capture
    value DECIMAL(4, 1) DEFAULT NULL,                   -- Valeur
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE wind (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    date DATETIME NOT NULL,                             -- Date de la capture
    value INTEGER(3) DEFAULT NULL,                      -- Valeur
    gust INTEGER(3) DEFAULT NULL,                       -- Rafale
    direction INTEGER(3) DEFAULT NULL,                  -- Direction
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE windchill (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    date DATETIME NOT NULL,                             -- Date de la capture
    value DECIMAL(3, 1) DEFAULT NULL,                   -- Valeur
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE humidex (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    date DATETIME NOT NULL,                             -- Date de la capture
    value DECIMAL(3, 1) DEFAULT NULL,                   -- Valeur
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE humidity (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    date DATETIME NOT NULL,                             -- Date de la capture
    value INTEGER(100) DEFAULT NULL,                    -- Valeur
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE snow (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,    -- Identifiant
    date DATETIME NOT NULL,                             -- Date de la capture
    value INTEGER(3) DEFAULT NULL,                      -- Valeur
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE levelling (
    id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,   -- Identifiant
    station INTEGER(10) UNSIGNED NOT NULL,             -- Identifiant de la station
    date DATETIME NOT NULL,                            -- Date de la capture
    value DECIMAL(6, 2),                               -- Valeur
    PRIMARY KEY(id)
) ENGINE=INNODB;

